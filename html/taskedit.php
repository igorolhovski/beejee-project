<?php
    include_once "../php/init.php";
    $task = $init->getTask($_GET['id']);

?>
<div class="ajaxcontent">
    <!-- Task edit form -->
    <form action="." method="POST">
        <input type="hidden" name="do" value="edittask">
        <input type="hidden" name="id" value="<?php print $task['id']; ?>">

        <input type="hidden" name="page" value="<?php print $init->getFilteredText($_GET['page']); ?>">
        <input type="hidden" name="orderby" value="<?php print $init->getFilteredText($_GET['orderby']); ?>">
        <input type="hidden" name="order" value="<?php print $init->getFilteredText($_GET['order']); ?>">

        <div class="input-group form-group">
            <input name="name" type="text" class="form-control" placeholder="название" value="<?php print $task['name']; ?>">
        </div>
        <div class="input-group form-group">
            <input name="email" type="text" class="form-control" placeholder="email" value="<?php print $task['email']; ?>">
        </div>
        <div class="input-group form-group">
            <textarea name="body" class="form-control" placeholder="содержание"><?php print $task['body']; ?></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Сохранить" class="btn float-right login_btn">
        </div>
    </form>
</div>