<?php
$tasks = $init->getTasks($_GET["page"], $_GET["orderby"], $_GET["order"]);
$page = 1;
?>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"><a
                        href="/?page=<?php print $tasks["current_page"]; ?>&orderby=name&order=<?php if ($tasks["order"] == "ASC") {
                            print "DESC";
                        } else {
                            print "ASC";
                        } ?>">Имя</a>
                </th>
                <th scope="col"><a
                        href="/?page=<?php print $tasks["current_page"]; ?>&orderby=email&order=<?php if ($tasks["order"] == "ASC") {
                            print "DESC";
                        } else {
                            print "ASC";
                        } ?>">Email</a>
                </th>
                <th scope="col"><a
                        href="/?page=<?php print $tasks["current_page"]; ?>&orderby=body&order=<?php if ($tasks["order"] == "ASC") {
                            print "DESC";
                        } else {
                            print "ASC";
                        } ?>">Содержание</a>
                </th>
                <th scope="col"><a
                        href="/?page=<?php print $tasks["current_page"]; ?>&orderby=last_modified&order=<?php if ($tasks["order"] == "ASC") {
                            print "DESC";
                        } else {
                            print "ASC";
                        } ?>">Редактировано</a>
                </th>
                <?php if ($user) { ?>
                <th scope="col">Редактировать</th>
                <th scope="col">Выполнено</th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks["content"] as $task) { ?>
            <tr>
                <th scope="row"><?php print $task["id"]; ?>
                </th>
                <td><?php print $task["name"]; ?>
                </td>
                <td><?php print $task["email"]; ?>
                </td>
                <td><?php print $task["body"]; ?>
                </td>
                <td>
                <?php $last_modified = new DateTime($task["last_modified"]); ?>
                <?php print $last_modified->format("d.m.Y H:i:s"); ?>
                </td>
                <?php if ($user) { ?>
                <td><a href="#"
                        onclick="popup('html/taskedit.php?id=<?php echo $task['id']; ?>&page=<?php echo $tasks['current_page']; ?>&orderby=<?php echo $tasks['orderby']; ?>&order=<?php echo $tasks['order']; ?>', '.ajaxcontent', 'Редактирование задания')"><i
                            class="fas fa-edit"></a></i>
                </td>
                <td>
                    <form
                        action="html/taskflag.php?id=<?php echo $task['id']; ?>"
                        method="POST" class="formajax donotupdate">
                        <input type="hidden" name="do" value="flagtask">
                        <input name="flag" onchange="$(this).parent().submit();" type="checkbox" <?php if ($task["flag"] == 1) { ?>checked<?php } ?>>
                    </form>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php if ($tasks["pager"]) { ?>
<hr>
<nav aria-label="...">
    <ul class="pagination">
        <li
            class="page-item <?php if ($tasks["current_page"] == 1) { ?>disabled<?php } ?>">
            <a class="page-link"
                href="/?page=<?php print $tasks["current_page"]-1; ?>&orderby=<?php print $tasks["orderby"]; ?>&order=<?php print $tasks["order"]; ?>"
                tabindex="-1">Previous</a>
        </li>

        <?php while ($page <= $tasks["pager"]) { ?>
        <li
            class="page-item <?php if ($tasks["current_page"] == $page) { ?>active<?php } ?>">
            <a class="page-link"
                href="/?page=<?php print $page; ?>&orderby=<?php print $tasks["orderby"]; ?>&order=<?php print $tasks["order"]; ?>"><?php print $page; ?> <span
                    class="sr-only">(current)</span></a>
        </li>
        <?php $page++; } ?>
        <li
            class="page-item <?php if ($tasks["pager"] == $tasks["current_page"]) { ?>disabled<?php } ?>">
            <a class="page-link"
                href="/?page=<?php print $tasks["current_page"]+1; ?>&orderby=<?php print $tasks["orderby"]; ?>&order=<?php print $tasks["order"]; ?>">Next</a>
        </li>
    </ul>
</nav>
<?php }
