<?php
    include_once "../php/init.php";

?>
<div class="ajaxcontent">
    <?php if (!$user) { ?>
    <!-- Login form -->
    <form action="." method="GET">
        <input type="hidden" name="do" value="login">
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input name="username" type="text" class="form-control" required="required" placeholder="имя пользователя">
        </div>
        <div class="input-group form-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input name="password" type="password" class="form-control" required="required" placeholder="пароль">
        </div>
        <div class="form-group">
            <input type="submit" value="Зайти" class="btn float-right login_btn">
        </div>
    </form>
    <?php } ?>
    <?php if ($user) { ?>
    <!-- Logout form -->
    <h4>Вы вошли как: <b><?php echo $user; ?></b></h4>
    <form action="." method="GET">
        <input type="hidden" name="do" value="logout">
        <div class="form-group">
            <input type="submit" value="Выйти" class="btn float-right login_btn">
        </div>
    </form>
    <?php } ?>
</div>