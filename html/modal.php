<!-- The Modal -->
<div class="modal" id="modal">
  <div class="modal-dialog">
    <div class="modal-content text-light">

      <!-- Modal Header -->
      <div class="modal-header">
        <h3 class="modal-title"></h3>
        <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
      </div>
    </div>
  </div>
</div>