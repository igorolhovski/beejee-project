<?php
    include_once "../php/init.php";
?>

<div class="ajaxcontent">
    <!-- Add new task form -->
    <form action="." method="POST">
        <input type="hidden" name="do" value="addtask">

        <div class="input-group form-group">
            <input name="name" type="text" class="form-control" placeholder="название">
        </div>
        <div class="input-group form-group">
            <input name="email" type="text" class="form-control" placeholder="email">
        </div>
        <div class="input-group form-group">
            <textarea name="body" class="form-control" placeholder="содержание"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Добавить" class="btn float-right login_btn">
        </div>
    </form>
</div>