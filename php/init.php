<?php
include_once "classes.php";
$init = new Projects();
$user = $init->getAccount();
if (isset($_POST["do"])) {
    if ($_POST["do"] == "addtask") {
        $init->addTask();
    }
    if ($_POST["do"] == "edittask") {
        $init->editTask($_POST['id'], $_POST['page'], $_POST['orderby'], $_POST['order']);
    }
    if ($_POST["do"] == "flagtask") {
        $init->flagTask($_GET['id']);
    }
}