<?php

class Projects
{
    public function __construct()
    {
        /*
            Open SQL connection and create required tables
        */
        include_once(__DIR__."/configurations.php");
        $sql_connection = new mysqli($this->sql_host, $this->sql_user, $this->sql_pass, $this->sql_db);
        if ($sql_connection->connect_errno) {
            die($sql_connection->connect_error);
        }
        
        $create_tasks_sql = "CREATE TABLE IF NOT EXISTS `tasks` (
            `id` int(11) AUTO_INCREMENT NOT NULL,
            `name` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `body` text NOT NULL,
            `flag` tinyint(1) DEFAULT '0',
            `last_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            primary key (id)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $sql_connection->query($create_tasks_sql);

        $create_user_sql = "CREATE TABLE IF NOT EXISTS `user` (
            `id` int(11) AUTO_INCREMENT NOT NULL,
            `user` text NOT NULL,
            `pass` text NOT NULL,
            primary key (id)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $sql_connection->query($create_user_sql);

        $this->sql_connection = $sql_connection;
    }

    /*
        Close SQL connection
    */
    public function __destruct()
    {
        $this->sql_connection->close();
    }

    public function getTasks($page, $orderby, $order)
    {
        if (!$page) {
            $page = 1;
        }
        if (!$orderby) {
            $orderby = "id";
        }
        if (!$order) {
            $order = "ASC";
        }

        $sql_counter = "SELECT COUNT(*) FROM tasks";
        $tasks_counter = $this->sql_connection->query($sql_counter);
        $tasks_total = $tasks_counter->fetch_array()[0];

        $pages = round($tasks_total / $this->pagination_per_page);

        $page = (int) $this->getFilteredText($page);
        $pageoffset = $page * $this->pagination_per_page - $this->pagination_per_page;
        $sql = "SELECT * FROM tasks 
                ORDER BY ".$this->getFilteredText($orderby)." ".$this->getFilteredText($order)." 
                LIMIT ".$this->pagination_per_page." 
                OFFSET ".$pageoffset;
        $tasks = $this->sql_connection->query($sql);
        if (!$tasks) {
            $this->sendMessage("danger", "Ошибка!", "/");
            return false;
        }

        $tasks_result = array();
        while ($task = $tasks->fetch_assoc()) {
            $tasks_result[] = $task;
        }

        $pager = false;
        if ($pages > 1) {
            $pager = $pages;
        }

        return array(
            "pager" => $pager,
            "content" => $tasks_result,
            "current_page" => $page,
            "orderby" => $orderby,
            "order" => $order
        );
    }

    public function getTask($id)
    {
        $sql = "SELECT * FROM `tasks` 
                WHERE `id` = ".$this->getFilteredText($id);
        $task_query = $this->sql_connection->query($sql);
        while ($task = $task_query->fetch_assoc()) {
            return $task;
        }
    }

    public function editTask($id, $cur_page=false, $cur_orderby=false, $cur_order=false)
    {
        if (!$this->getAccount()) {
            $this->sendMessage("danger", "Доступ запрещён", "/");
            return false;
        }

        $name = $this->getFilteredText($_POST["name"], "not empty");
        $email = $this->getFilteredText($_POST["email"], "not empty");
        $body = $this->getFilteredText($_POST["body"], "not empty");
        
        if (!$name || !$email || !$body) {
            $this->sendMessage("danger", "Все поля обязательны", "/");
            return false;
        }

        if ($cur_page) {
            $redirect_link = "/?page=".$cur_page."&orderby=".$cur_orderby."&order=".$cur_order;
        } else {
            $redirect_link = "/";
        }

        $sql = "UPDATE `tasks` 
                SET `name` = '".$name."', `email` = '".$email."', `body` = '".$body."', `last_modified` = now() 
                WHERE `id` = ".$this->getFilteredText($id);
        $this->sql_connection->query($sql);
        $this->sendMessage("success", "Задача ".$name." обновлена.", $redirect_link);
    }

    public function flagTask($id)
    {
        if (!$this->getAccount()) {
            $this->sendMessage("danger", "Доступ запрещён", "/");
            return false;
        }

        $flag = $this->getFilteredText($_POST["flag"]);
        if ($flag == "on") {
            $flag = 1;
        }

        $sql = "UPDATE `tasks` 
                SET `flag` = '".$flag."' 
                WHERE `id` = ".$this->getFilteredText($id);

        $this->sql_connection->query($sql);
        $this->sendMessage("success", "Задача ".$name." обновлена.", "/");
    }

    public function addTask()
    {
        $name = $this->getFilteredText($_POST["name"], "not empty");
        $email = $this->getFilteredText($_POST["email"], "not empty");
        $body = $this->getFilteredText($_POST["body"], "not empty");

        if (!$name || !$email || !$body) {
            $this->sendMessage("danger", "Все поля обязательны", "/");
            return false;
        }

        $sql = "INSERT INTO `tasks` (`name`, `email`, `body`)
                VALUES ('".$name."', '".$email."', '".$body."')";

        $this->sql_connection->query($sql);
        $this->sendMessage("success", "Задача ".$name." добавлена.", "/");
    }

    public function getMessage()
    {
        $redirect = false;
        if (isset($_COOKIE['msg_code'])) {
            if (isset($_COOKIE['redirect'])) {
                $redirect = $this->getFilteredText($_COOKIE['redirect']);
            }
            $msg_code = $this->getFilteredText($_COOKIE['msg_code']);
            $msg_text = $this->getFilteredText($_COOKIE['msg_text']);
            setcookie("msg_code", "", time() + (86400 * 30), "/");
            setcookie("msg_text", "", time() + (86400 * 30), "/");
            setcookie("redirect", "", time() + (86400 * 30), "/");
            return array(
                "code" => $msg_code,
                "text" => $msg_text,
                "redirect" => $redirect
            );
        }
        return false;
    }

    public function sendMessage($code, $text, $redirect)
    {
        setcookie("msg_code", $code, time() + (86400 * 30), "/");
        setcookie("msg_text", $text, time() + (86400 * 30), "/");
        if ($redirect) {
            setcookie("redirect", $redirect, time() + (86400 * 30), "/");
            Header("Location: ".$redirect);
        }
    }

    public function getAccount()
    {
        session_start();

        if ((!isset($_SESSION['username']) && !isset($_SESSION['password']))
        &&
        (!isset($_GET['username']) && !isset($_GET['password']))
     ) {
            return false;
        }

        if ($_GET["do"] == "logout") {
            unset($_SESSION['username']);
            unset($_SESSION['password']);
            $this->sendMessage('success', 'Выход успешен', "/");
        }

        if ($_GET["do"] == "login") {
            $ask_user = $this->getFilteredText($_GET['username']);
            $ask_pass = $this->getFilteredText($_GET['password']);
            $sql = "SELECT * FROM `user` 
                WHERE `user` = '".$ask_user."' 
                AND `pass` = '".$this->getHash($ask_pass)."'";
            $found_user_query = $this->sql_connection->query($sql);
            if ($found_user_query->num_rows > 0) {
                while ($found_user = $found_user_query->fetch_assoc()) {
                    $_SESSION['username'] = $found_user["user"];
                    $_SESSION['password'] = $found_user["pass"];
                    $this->sendMessage('success', 'Вход успешен', "/");
                }
            } else {
                $this->sendMessage('danger', 'Пароль неправильный', "/");
            }
        }

        $found_user = $_SESSION['username'];
        return $found_user;
    }

    public function getFilteredText($string, $validation=false)
    {
        $string = htmlspecialchars($this->sql_connection->real_escape_string($string), ENT_QUOTES);
        if ($validation && $string == "") {
            return false;
        }
        return $string;
    }

    public function getHash($string)
    {
        return hash('sha256', hash('sha256', $string));
    }
}
