function bindform() {


  $(".modal-body form, .formajax").each(function () {
    $(this).submit(function (event) {

      $(this).find("input[type='submit']").prop('disabled', true);
      var element = $(this);
      element.append('<img width="35" class="formajaxer" src="/img/ajaxer.svg">');
      event.preventDefault();
      var target = $(this).attr("action");
      var met = $(this).attr("method");
      var FormData = $(this).serialize();

      $.ajax
        ({
          type: met,
          url: target,
          data: FormData,

          success: function (html) {

            var content = $(html).filter(".container").children();
            var nav = $(html).filter("nav").children();
            var message = content.find(".message");

            var container = "body > .container";
            var container_nav = "body > nav";

            if (message.attr("data-code") == "danger") {
              element.find(".message").remove();
              element.prepend(message);
              element.find(".formajaxer").remove();
              element.find("input[type='submit']").prop('disabled', false);
              return false;
            }
            if (element.hasClass("donotupdate")) {
              $(container).find(".formajaxer").remove();
              $(container).find(".message").remove();
              $(container).prepend(message);
            }
            else {
              $(container).html(content);
              $(container_nav).html(nav);
              element.find("input[type='submit']").prop('disabled', false);
              bindform();
              $('#modal').modal('hide');
            }
          },
        }).fail(function () {
          location.reload();
        });

    });
  });
}


function popup(url, toload, title) {
  $(".modal-title").text(title);
  var toshow = ".modal-body";

  if (toload) {
    url = url + " " + toload;
    $(toshow).load(url, function (response, status, xhr) {
      if (status == "error") {
        var msg = "Sorry but there was an error: ";
        $("#error").html(msg + xhr.status + " " + xhr.statusText);
      }
      $(".modal-body").css("background-image", "none");
      bindform();
    })
  }

  else if (!toload && url != "") {
    $(toshow).html('<iframe width="100%" style="height:500px; border: 0px none;" frameBorder="0" src="' + url + '"></iframe>');
    $(".modal-body").css("background-image", "none");
  }

  else {
    $(".modal-body").css("background-image", "none");
  }


  $('#modal').modal('show');

  $(toshow).css("display", "block");
  $(".overlay").css("top", $(window).scrollTop() + "px");
  $(".close-overlay").click(function () {
    $(".overlay").css("display", "none");
    $(toshow).css("display", "none");
  });
}

bindform();