<?php
    include_once "php/init.php";
?>
<!DOCTYPE html>
<html lang="ru">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1,
      shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>BeeJee project</title>

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Font awesome CSS -->
  <link href="lib/fa/css/all.min.css" rel="stylesheet">

  <!-- Project's CSS -->
  <link href="css/main.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <?php if ($user) { ?>
      <a class="navbar-brand" href="#">Добро пожаловать, <?php echo $user; ?></a>
      <?php } ?>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Начальная страница
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" onclick="popup('html/task.php', '.ajaxcontent', 'Добавление задания')">Добавить
              задание</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"
              onclick="popup('html/account.php', '.ajaxcontent', 'Профиль пользователя')">Профиль</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <?php if ($message = $init->getMessage()) { ?>
        <div
          data-code="<?php echo $message["code"]; ?>"
          class="message alert mt-2 alert-<?php echo $message["code"]; ?>"
          role="alert">
          <?php echo $message["text"]; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-left">
        <h3 class="mt-5">Список задач</h3>

        <?php include_once("html/tasks.php"); ?>
      </div>
    </div>
  </div>

  </div>

  <?php include_once('html/modal.php'); ?>

  <!-- Bootstrap core JavaScript -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="lib/bootstrap/js/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Project's js -->
  <script src="js/main.js"></script>

</body>

</html>